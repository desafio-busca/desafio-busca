# Desafio busca

O desafio foi implementado visando um código simples mas que atenda aos requisitos informados.

## Arquitetura dos arquivos

- search.py - Arquivo principal para obter o resultado da busca
- src/processamento.py - Arquivo para realizar o pré-processamento
- src/data/files - Pasta onde estão os arquivos .txt sem nenhum processamento
- src/data/jsons - Pasta onde ficam os arquivos .json que estão processados para o arquivo principal fazer as buscas de maneira mais eficiente
- src/tests/teste.py - Arquivo para realizar os testes

## Requisitos necessários e passos para rodar o algoritmo de buscas

- Instalação Python 3.6
- Pré-processamento dos dados em .txt
- Testes
- Rodar algoritmo do desafio

## Instalação

- Instalar python3.6 no ambiente de desenvolvimento

```sh
$ sudo apt update
$ sudo apt install python3.6
``` 

## Pré-processamento dos dados em .txt

O pré-processamento é realizado de acordo com os arquivos .txt encontrados em "src/data/files" após o processamento são criados arquivos .json para cada uma das palavras encontradas, dentro de cada um desses arquivos são salvos os nomes do arquivo .txt já em ordem crescente alfabética.

- Na pasta inicial do projeto rodar:

```sh
python3 src/processamento.py
```

## Testes

Foram feitos 3 testes:
- 1º - Verificar se o tempo da busca está menor que 1 ms
- 2º - Verificar se a quantidade do resultado está correta(foram usadas palavras que já era conhecido o resultado para teste)
- 3º - Verificar se o pré-processamento está funcionando de forma adequada

Na pasta inicial do projeto rodar:

```sh
python3 src/tests/teste.py
```

## Rodar algoritmo do desafio

Na pasta inicial do projeto rodar:

```sh
python3 search.py walt disney
```

Obs: Para fazer a pesquisa das palavras é necessário passar ambas após o arquivo .py separadas com espaço como no exemplo acima.

## Observações finais

Ao decorrer da implementação do desafio foram implementados outros tipos de algoritmos mas nenhum teve um resultado de resposta em tempo melhor que o mencionado acima, acredito que é possível chegar em resultados melhores com um pouco mais de estudo.
