import sys
import os
import json
from datetime import datetime
from datetime import timedelta
import pdb

class Busca:
    def __init__(self, palavra_1, palavra_2):
        self.palavra_1 = palavra_1
        self.palavra_2 = palavra_2
        self.dir_path = os.path.dirname(os.path.abspath(__file__)) + '/src/data/jsons/'

    def palavras(self):
        print("palavras", self.palavra_1 + ", " + self.palavra_2)
        
        try:
            f = open(self.dir_path + self.palavra_1 + '.json')
            lista_palavra_1 = json.load(f)
            lista_palavra_1 = lista_palavra_1['files']
        except:
            lista_palavra_1 = []

        try:
            f = open(self.dir_path + self.palavra_2 + '.json')
            lista_palavra_2 = json.load(f)
            lista_palavra_2 = lista_palavra_2['files']
        except:
            lista_palavra_2 = []
        
        return set(lista_palavra_1).intersection(lista_palavra_2)


if __name__ == "__main__":
    start_time = datetime.now()

    palavra_1 = str(sys.argv[1])
    palavra_2 = str(sys.argv[2])

    busca = Busca(palavra_1, palavra_2)
    busca_palavras = busca.palavras()

    print('Foram encontradas ' + str(len(busca_palavras)) + ' ocorrências pelo termo "' + palavra_1 + ' ' + palavra_2 + '"')
    if(len(busca_palavras) > 0):
        print('Os arquivos que possuem "' + palavra_1 + ' ' + palavra_2 + '" são:')
        for arquivo in busca_palavras:
            print(arquivo)
    
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    print(ms)