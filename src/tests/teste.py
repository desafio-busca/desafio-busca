import unittest
import pdb
import sys
import os
from datetime import datetime
from datetime import timedelta

sys.path.append("./..") 
from desafio.search import Busca
from desafio.src.processamento import Processamento


class BuscaTeste(unittest.TestCase):
    def test_busca_tempo(self):
        for i in range(2):
            start_time = datetime.now()

            busca = Busca("walt", "disney")
            resultado = busca.palavras()

            dt = datetime.now() - start_time
            ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0

            isTempo = False
            if(ms < 1):
                isTempo = True

        self.assertTrue(isTempo)

    def test_busca_arquivos(self):
        busca = Busca("walt", "disney")
        resultado = busca.palavras()
        
        self.assertEqual(len(resultado), 53)

class ProcessamentoTeste(unittest.TestCase):
    def test_processamento(self):
        processamento = Processamento()
        self.assertTrue(processamento)

if __name__ == "__main__":
    unittest.main()