import os
import json
import time

class Processamento:
    def __init__(self):
        self.dir_path = os.path.dirname(os.path.abspath(__file__)) + '/data/'

    def json(self):
        try:
            dir_path_files = self.dir_path + 'files'
            dir_path_json = self.dir_path + 'jsons'
            new_json = {}
            
            for file in os.listdir(dir_path_files):
                cur_path = os.path.join(dir_path_files, file)

                if file.endswith(".txt"):
                    with open(cur_path, 'r') as f:
                        documento = f.read()
                    
                    for word in documento.split(' '):
                        word = ''.join(char for char in word if char.isalnum())
                        if(word != ''):
                            try:
                                ff = open(dir_path_json + '/' + word + '.json')
                                data = json.load(ff)
                                data['files'].append(file)
                                data['files'].sort()
                            except:
                                data = {
                                    'files': [file]
                                }
                            
                            with open(dir_path_json + '/' + word + '.json', 'w',  encoding='utf-8') as f:
                                json.dump(data, f, ensure_ascii=False, indent=4)

            return True
        
        except:
            return False

    def clear_folder(self):
        dir = self.dir_path + 'jsons'
        if os.path.exists(dir):
            for the_file in os.listdir(dir):
                file_path = os.path.join(dir, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                    else:
                        clear_folder(file_path)
                        os.rmdir(file_path)
                        return True
                except Exception as e:
                    return False


if __name__ == "__main__":
    processamento = Processamento()
    processamento.clear_folder()
    time.sleep(3)
    resultado = processamento.json()
    print(resultado)
